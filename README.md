# Krita Knockback

Implements a knockback function to Krita. Knockback lowers the opacity of all pixels on the current layer, allowing you to draw over your lines without needing to make a new layer or erasing everything.

![](kb.webm)

## Installation

* Find the resources folder by going to `Settings > Manage Resources...` and clicking `Open Resource Folder`.
* Copy `knockback` and `knockback.desktop` to the `pykrita` directory in the resources folder.
* Copy `knockback.action` to the `actions` directory in the resources folder.
* Restart Krita, and enable the plugin in the Python Plugin Manager in `Settings > Configure Krita...`

## Notes

Currently works with 8 bit integer colors. Not planning on extending this to other formats.
