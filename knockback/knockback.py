# Knockback function for Krita

from krita import *
import struct as st

class Knockback(Extension):
	def knockback(self):
		document = Krita.instance().activeDocument()
		layer = document.activeNode()
		sel = document.selection()
		if sel:
			sx, sy, sw, sh = sel.x(), sel.y(), sel.width(), sel.height()
			selmask = sel.pixelData(sx, sy, sw, sh)
		else:
			bounds = layer.bounds()
			sx, sy, sw, sh = bounds.x(), bounds.y(), bounds.width(), bounds.height()
			selmask = QByteArray(sw*sh,b'\xFF')
			
		pixeldata = layer.pixelData(sx, sy, sw, sh)
		newpixdata = QByteArray(sw*sh*4, '0')

		for idx in range(sw*sh):
			offset = idx*4
			s = selmask[idx][0]
			b, g, r, a = st.unpack_from('BBBB', pixeldata, offset)
			a = int(a - (float(a)*s)/(510))
			st.pack_into('BBBB', newpixdata, offset, b, g, r, a)

		layer.setPixelData(newpixdata, sx, sy, sw, sh)
		document.refreshProjection()

	def __init__(self, parent):
		super().__init__(parent)

	def setup(self):
		pass

	def createActions(self, window):
		action = window.createAction("Knockback")
		action.triggered.connect(self.knockback)

Krita.instance().addExtension(Knockback(Krita.instance()))
